#include<stdio.h>
void swap(int *x,int *y)
{
*x=*x+*y;
*y=*x-*y;
*x=*x-*y;
}
int main()
{
int x,y;
    printf("Enter two integers to swap\n");
    scanf("%d %d",&x,&y);
    printf("Numbers before swapping => %d  %d\n",x,y);
    swap(&x,&y) ;
    printf("\nNumbers after swapping => %d  %d",x,y);
    return 0;
}