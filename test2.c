#include <stdio.h>

float area(float r);

int main()
{
  float r;
    printf("Enter the value of radius: \n");
    scanf (" %f",&r);

    printf("the area of circle is %.3f",area(r));

    return 0;
}

float area(float r)

{
    return (3.142*r*r);
}
